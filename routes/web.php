<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('development', function () {
	
	$database = DB::table('databases')->get();

    return view('development',compact('database'));
});


Route::get('','MainController@index')->name('page.main');

Route::get('home','HomeController@index')->name('page.home');

Route::get('post' , 'PostController@index')->name('page.posts');
Route::get('post/create' , 'PostController@create')->name('post.create');
Route::get('post/show/{id}' , 'PostController@show')->name('post.show');
Route::get('post/edit/{id}' , 'PostController@edit')->name('post.edit');
Route::post('post' , 'PostController@store')->name('post.store');
Route::put('post/{id}' , 'PostController@update')->name('post.update');
Route::get('post/{id}' , 'PostController@destroy')->name('post.delete');

Route::get('about' , 'AboutController@index')->name('page.about');

Route::get('contact' , 'ContactController@index')->name('page.contact');

