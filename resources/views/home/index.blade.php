@extends('layout')

@section('title' , ' | Home')


@section('content')
      <div class='row'>
        <div class="col-md-12">
            <div class='jumbotron'>
              <h1>Welcome to my blog</h1>
              <p>Thanks for visiting</p>
              <p><a class="btn btn-primary btn-lg" href="#">Submit</a></p>
            </div>
        </div>
      </div>


      <div class="rov">
        <div class="col-md-8">
          <div class='post'>
            <h3>Post Title</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia voluptatem dolor qui iusto ex aliquam alias repellat magni temporibus nam omnis facilis nemo, earum fugit. Ea, unde nihil. Hic, accusantium.</p>
            <a href="#" class='btn btn-primary'>read more</a>

            <hr>
          </div>
        </div>

        <div class="col-md-3 col-md-offset-1">
          <h2>SideBar</h2>
        </div>
      </div>
@endsection