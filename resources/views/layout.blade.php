<!DOCTYPE html>
<html lang="en">
  <head>
      @include('_head')
  </head>
  <body>
    @include('_nav')
  
    <!--start  container -->
    <div class='container'>
      @include('_messages')
      @yield('content')
      @include('_footer')
    </div>
    <!-- end container -->
    
    @include('_javascript')
  </body>
</html>