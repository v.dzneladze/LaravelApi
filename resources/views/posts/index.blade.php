@extends('layout')

@section('title' , ' | Post')

@section('content')
	<div class="row">
        <div class="col-md-12">
    		@foreach($data as $post)
          <div class='post'>
            <h3>{{ $post->title }}</h3>
            <p>{{$post->body}}.</p>
            <a href="{{ route('post.show',$post->id) }}" class='btn btn-primary'>read more</a>
            <a href="{{ route('post.edit',$post->id) }}" class='btn btn-success'>update post</a>
            <a href="{{ route('post.delete',$post->id) }}" class='btn btn-danger'> delete</a>
            <hr>
          </div>
         @endforeach
        </div>

        <hr>
        {!! $data->links() !!}
        <a href='{{ route('post.create') }}' class='btn btn-primary' >add new posts</a>
@endsection