@extends('layout')

@section('title' , ' | Update  post')

@section('content')
	<div class="row">
    <div class='col-md-8 col-md-offset-2'>
      <h1>Create New Post</h1>
      <hr>
    
      {!! Form::open([ 'method'  => 'put', 'route' => [ 'post.update', $data->id ] ]) !!}
        {{ Form::label('title','Title:')}}
        {{ Form::text('title',$data->title, array('class'=>'form-control') )}}

        {{ Form::label('slug','Slug:')}}
        {{ Form::text('slug',$data->slug, array('class'=>'form-control') )}}

        {{ Form::label('body', 'Body:')}}
        {{ Form::textarea('body', $data->body, array('class'=>'form-control') )}}

        {{ Form::submit('Create Post', array('class'=>'btn btn-success btn-lg btn-block','style'=>'margin-top:20px'))}}
      {!! Form::close() !!}
    </div>
  </div>
@endsection