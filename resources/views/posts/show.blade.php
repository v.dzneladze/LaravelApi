@extends('layout')

@section('title' , ' | Show Post')

@section('content')
	<div class="row">
        <div class="col-md-12">
          <div class='post'>
            <h3>{{ $data->title }}</h3>
            <p>{{$data->body}}.</p>
            <hr>
          </div>
        </div>

        <hr>

        <a href='{{ route('post.create') }}' class='btn btn-primary' >add new posts</a>
  </div>
@endsection