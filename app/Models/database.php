<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class database extends Model
{
    protected $table = 'databases';


    public $transformFields = [
        'num_rows' => 'numRows',
    ];

}
