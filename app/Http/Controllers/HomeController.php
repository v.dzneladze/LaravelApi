<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
    	$pageArray = [
    		'title' => 'home',
    		'content' => 'whole data and something like that for first page',
    		];
    	return view('home.index')->withData($pageArray);
    }
    
}
